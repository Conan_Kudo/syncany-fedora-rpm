%define releasetag alpha
Name:           syncany
Version:        0.4.6
Release:        1%{?dist}
Summary:        Cloud storage/filesharing app focusing on security and storage abstraction

License:        GPLv3+
URL:            https://www.syncany.org/
Source0:        https://github.com/%{name}/%{name}/archive/%{version}-%{releasetag}.tar.gz#/%{name}-%{version}-%{releasetag}.tar.gz
Patch0:         syncany-disable-tests.patch

BuildRequires:  gradle-local

BuildRequires:  mvn(commons-io:commons-io)
BuildRequires:  mvn(commons-codec:commons-codec)
BuildRequires:  mvn(org.bouncycastle:bcprov-jdk15on)
BuildRequires:  mvn(org.bouncycastle:bcpkix-jdk15on)
BuildRequires:  mvn(org.simpleframework:simple-xml)
BuildRequires:  mvn(com.google.guava:guava)
BuildRequires:  mvn(org.hsqldb:hsqldb)
BuildRequires:  mvn(com.github.zafarkhaja:java-semver)
BuildRequires:  mvn(io.undertow:undertow-servlet)
BuildRequires:  mvn(io.undertow:undertow-websockets-jsr)
BuildRequires:  mvn(net.sf.jpathwatch:jpathwatch)
BuildRequires:  mvn(org.apache.httpcomponents:httpclient)
BuildRequires:  mvn(com.google.code.gson:gson)
BuildRequires:  mvn(net.sf.jopt-simple:jopt-simple)
BuildRequires:  mvn(org.jboss.apiviz:apiviz)

%description
Syncany is an open-source cloud storage and filesharing application.
It allows users to backup and share certain folders of their workstations
using any kind of storage, e.g. FTP, SFTP, WebDAV, Amazon S3 and Samba.

While the basic idea is similar to Dropbox, Syncany is open-source and
additionally provides data encryption and more flexibility in terms of
storage type and provider:

* Data encryption: Syncany encrypts the files locally, so that any
                   online storage can be used even for sensitive data.
* Any storage: Syncany uses a plug-in based storage system. It can
               be used with any type of remote storage.


%package javadoc
Summary:        Javadocs for %{name}

%description javadoc
This package contains the API documentation for %{name}


%prep
%setup -q -n %{name}-%{version}-%{releasetag}
%patch0
find -name \*.jar -delete
find -name \*.class -delete

%build
# What is the thing to do for gradle here?
gradle-local installDist --offline -x test -x testAll -x testAllButLongCrypto -x testAllLong -x testGlobal -x testScenario

%install
rm -rf $RPM_BUILD_ROOT
%mvn_install


%files
%doc AUTHORS.md CHANGELOG.md README.md
%license LICENSE-GPLv3 LICENSE.md


%files javadoc -f .mfiles-javadoc

%changelog
* Sat Jun 20 2015 Neal Gompa <ngompa13{%}gmail{*}com> - 0.4.6-1
- Initial packaging
